package nxjsim.AR;

/**
 * Created by kuba on 2.11.14.
 */
public class FoundException extends Exception {
    @Override
    public Throwable fillInStackTrace() {
        return this;
    }
}