package nxjsim.core;

import lejos.nxt.NXTRegulatedMotor;

/**
 * Motor event listener
 * @author Jakub Vaněk
 */
public interface EventListener {
    public void MotorModified(NXTRegulatedMotor motor, NXTRegulatedMotor.MotorEvent e, int value);
    public int getUSvalue();
    public boolean getTSpress();
    public int getGyroDirection();
    public void buttonPress();
}
