package lejos.nxt;
import nxjsim.core.EventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuba on 2.11.14.
 */
public class TouchSensor {
    public static List<EventListener> listeners = new ArrayList<>();
    public static void addListener(EventListener e)
    {
        listeners.add(e);
    }
    public TouchSensor(SensorPort a)
    {}
    public boolean isPressed()
    {
        return listeners.get(0).getTSpress();
    }
}
