package lejos.nxt;

/**
 * Created by kuba on 2.11.14.
 */
public class Motor {
    private Motor(){}
    public static final NXTRegulatedMotor A = new NXTRegulatedMotor("A"); // A motor
    public static final NXTRegulatedMotor B = new NXTRegulatedMotor("B"); // B motor
    public static final NXTRegulatedMotor C = new NXTRegulatedMotor("C"); // C motor
}
