package lejos.nxt;

import nxjsim.core.EventListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Button class.
 * @author Jakub Vaněk
 */
public class Button {
    public static List<EventListener> listeners = new ArrayList<EventListener>();
    public static void addListener(EventListener e)
    {
        listeners.add(e);
    }
    public static void waitForAnyPress()
    {
        listeners.get(0).buttonPress();
    }}
