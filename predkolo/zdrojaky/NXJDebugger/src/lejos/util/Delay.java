package lejos.util;

/**
 * Created by kuba on 2.11.14.
 */
public final class Delay {
    private Delay()
    {}
    public static void msDelay(int delay)
    {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
