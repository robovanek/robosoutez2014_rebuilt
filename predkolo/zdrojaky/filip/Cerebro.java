/**
* Main class. The brain
* @author Filip Smola (smola.filip@hotmail.com)
**/
public class Cerebro{
        //the threads
        private final MovementThread movement;
        private final SensorThread sensor;
        
        //the enums
        public enum State{
                EXPLORING, TARGETING, DONE
        };
    
        //the robot attributes
        private Position target;
        private State state;
        
        public static void main(String[] args){
                Cerebro cerebro = new Cerebro();
                cerebro.run();
        }
        
        private void run(){
                movement = new MovementThread();
                sensor = new SensorThread();
                state = EXPLORING;
                
                movement.run();
                sensor.run();
                
                while(state != DONE){
                        //TO-DO
                }
        }
}