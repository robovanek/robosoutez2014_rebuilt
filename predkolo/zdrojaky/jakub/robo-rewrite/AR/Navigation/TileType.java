package AR.Navigation;

/**
 * Výčet pole/bloku
 * @author Jakub Vaněk
 */
public class TileType {
	private TileType() {} // zbytečný ale nutný konstruktor

	/**
	 * Neobjevený blok
	 */
	public static final int Unknown = 0;
	/**
	 * Překážka
	 */
	public static final int Wall = 1;
	/**
	 * Průjezd, svítící světlo
	 */
	public static final int LightOn = 2;
	/**
	 * Průjezd, zhaslé světlo
	 */
	public static final int LightOff = 3;
}
