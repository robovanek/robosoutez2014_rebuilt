package AR;

import AR.Navigation.Map;
import AR.Navigation.MapOrientation;
import AR.Robotics.Mechanics;
import AR.Sensors.Ultrasonic;
import lejos.nxt.*;

/**
 * Kontejner obsahující Navigation a Robotics pro ovládání robota, funkce pro Main
 */
public class Controller {
	/**
	 * Předá mapu a robota, inicializuje senzory podle hodnot v Con0st
	 * @param map Mapa
	 * @param mech Mechanika
	 */
	public Controller(Map map, Mechanics mech)
	{
		this.map = map;
		this.mech = mech;
		this.sonic = new UltrasonicSensor(Const.sonicPort);
		this.touch = new TouchSensor(Const.touchPort1);
		this.touchB = new TouchSensor(Const.touchPort2);
		this.sonicctl = new Ultrasonic(this);
	}

	/**
	 * Virtuální mapa
	 */
	public Map map;
	/**
	 * Fyzické rozhraní robota
	 */
	public Mechanics mech;
	/**
	 * Ultrazvukový senzor
	 */
	public UltrasonicSensor sonic;
	/**
	 * Tlačítko 1 pro detekci kolize se stěnou
	 */
	public TouchSensor touch;
	/**
	 * Tlačítko 2 pro detekci kolize se stěnou
	 */
	public TouchSensor touchB;
	/**
	 * Ovladač ultrazvukového senzoru
	 */
	public Ultrasonic sonicctl;

	/**
	 * Otočí robota o 90° doleva
	 */
	public void turnLeft(){
		turnLeft(1);
	}

	/**
	 * Otočí robota o 90° doprava
	 */
	public void turnRight(){
		turnRight(1);
	}

	/**
	 * Otočí robota o count*90° doleva
	 */
	public void turnLeft(int count){
		mech.turn(false, count*90);
		map.turn(false, count);
	}

	/**
	 * Otočí robota o count*90° doprava
	 */
	public void turnRight(int count){
		mech.turn(true, count*90);
		map.turn(true, count);
	}

	/**
	 * Otočí se do daného směru
	 * @param orient nový směr
	 */
	public void turnTo(int orient) {
		int diff = MapOrientation.difference(map.curOrient, orient)*90; // návrat zpět
		if(diff!=0)
			mech.turn(diff>0,Math.abs(diff));
		map.curOrient = orient;
	}
	/**
	 * Posune se o pole dopředu
	 */
	public void forward(){
		forward(1);
	}

	/**
	 * Posune se o count polí dopředu
	 */
	public void forward(int count){
		mech.travel(false, Const.TILE_LENGTH*count, Const.FORWARDSPEED);
		map.travel(count, false);
	}

	/**
	 * Zjistí jestli jsou tlačítka stisknuta
	 * @param AND jestli použít AND nebo OR
	 * @return stav tlačítek
	 */
	public boolean touchPressed(boolean AND)
	{
		if(AND)
			return touch.isPressed() && touchB.isPressed();
		else
			return touch.isPressed() || touchB.isPressed();
	}

}
