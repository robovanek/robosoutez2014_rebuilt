package AR.Tests;

import lejos.nxt.*;
import lejos.util.Delay;

/**
 * Samostatná testovací implementace blikače
 */
public class Lights {
	/**
	 * Hlavní funkce
	 * @param args nic
	 */
    public static void main(String[] args)
    {
        NXTMotor light = new NXTMotor(MotorPort.C); // definujeme výstup
        light.setPower(10); // nastavíme startovní výkon
        int power = 10; // aktuální výkon
        boolean up = true; // kam míříme?
        LCD.clear();
        //UltrasonicSensor s = new UltrasonicSensor(SensorPort.S1);
        //TouchSensor minus = new TouchSensor(SensorPort.S3);
        //TouchSensor plus = new TouchSensor(SensorPort.S4);
        while(Button.ESCAPE.isUp()) // dokud nedržíme ESCAPE
        {
            /*
            int dist = s.getDistance();
            float range = s.getRange();
            LCD.drawString("DIST="+Integer.toString(dist)+"   ",0,0);
            LCD.drawString("RANGE="+Float.toString(range)+"   ",0,1);
            Delay.msDelay(200);
            */

            if(up) // pokud jdeme nahoru
            {
                if(power < 100) // pokud jsme v limitu
                    power+=5; // jdi dál
                else // jinak
                    up=false; // jdeme dolů
            }
            else // pokud jdeme dolů
            {
                if(power > 0) // pokud jsme v limitu
                    power-=5; // jdi dál
                else // jinak
                    up=true; // jdeme nahoru
            }
            light.setPower(power); // nastav výkon
            Delay.msDelay(10); // čekej

            /*
            if(up)
            {
                light.setPower(100);
                up=false;
            }
            else
            {
                light.setPower(0);
                up=true;
            }
            Delay.msDelay(200);
            */
        }
        // plynulé zhasnutí
        while(power<0) // dokud není světlo zhaslé (výkon = 0)
        {
            power--; // sniž výkon
            light.setPower(power); // nastav snížený výkon
            Delay.msDelay(10); // čekej
        }
    }
}
