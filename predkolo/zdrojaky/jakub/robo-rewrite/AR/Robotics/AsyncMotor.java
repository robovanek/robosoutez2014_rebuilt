package AR.Robotics;

import lejos.nxt.NXTRegulatedMotor;

/**
 * Třída pro asynchronní otáčení motorů
 * TODO překopat, udělat trackování aktuální ujeté vzdálenosti, udělat další vlákno pro sledování zaseknutí, viz FIXME.txt
 */
public final class AsyncMotor extends Thread
{
	/**
	 * Motor k otáčení
	 */
	private NXTRegulatedMotor motor;
	/**
	 * o kolik motor otočit
	 */
	private int rotation = 360;

	/**
	 * Konstruktor
	 * @param m motor k otočení
	 * @param degrees úhel o kolik otočit
	 */
	private AsyncMotor(NXTRegulatedMotor m, int degrees)
	{
		motor = m; // nastavíme motor
		rotation = degrees;	// a otočení
	}

	/**
	 * Otočí asynchronně daný motor o daný počet stupňů
	 * @param m motor k otáčení
	 * @param degrees úhel o kolik otáčet
	 * @return instance asynchronního motoru
	 */
	public static AsyncMotor rotate(NXTRegulatedMotor m, int degrees)
	{
		AsyncMotor r = new AsyncMotor(m,degrees); // vytvoření nového vlákna
		r.start(); // start vlákna
		return r; // return vlákna
	}

	/**
	 * Práce vlákna
	 */
	@Override
	public void run()
	{
		// otočit motorem
		motor.rotate(rotation);
	}
	/**
	 * Počká na ukončení asynchronní operace
	 *
	 * @param l Levý motor
	 * @param r Pravý motor
	 * @return Úspěch
	 */
	public static boolean waitFor(AsyncMotor l, AsyncMotor r)
	{
		try{
			l.join(); // počkat na konec threadu
			r.join(); // počkat na konec threadu
			//l.motor.waitComplete(); // TODO TESTOVAT
			//r.motor.waitComplete(); // TESTOVAT
		}catch(InterruptedException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
