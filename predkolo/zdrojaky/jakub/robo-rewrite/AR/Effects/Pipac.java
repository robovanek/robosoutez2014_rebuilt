package AR.Effects;

import lejos.nxt.Sound;

/**
 * Pípač pro ukončení jízdy
 */
public final class Pipac {
	private Pipac(){}
	/**
	 * Přehraje ukončovací melodii
	 */
	public static void playTune() {
		int time = 200;
		//int wait = 10;
		int[] instrument = Sound.FLUTE;
		// nahoru
		Sound.playNote(instrument, 500, time);
		//Delay.msDelay(wait);
		Sound.playNote(instrument, 700, time);
		//Delay.msDelay(wait);
		Sound.playNote(instrument, 900, time);
		//Delay.msDelay(wait);
		Sound.playNote(instrument, 1100, time);
		//Delay.msDelay(wait);

		// znovu nahoru ale výš
		Sound.playNote(instrument, 600, time);
		//Delay.msDelay(wait);
		Sound.playNote(instrument, 800, time);
		//Delay.msDelay(wait);
		Sound.playNote(instrument,1000, time);
		//Delay.msDelay(wait);
		Sound.playNote(instrument, 1200, time);
		//Delay.msDelay(wait);
		Sound.beepSequenceUp(); // nahoru a dolu, ale rychleji
		Sound.beepSequence();
	}
}
